/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { 
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Text,
  Button,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
//import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import { createDrawerNavigator } from '@react-navigation/drawer';

/**
 * @desc components for dashboard scree
 */
import InvoiceComponent from './screens/dashboard/views/invoiceComponent';
import CreditDetailsComponent from './screens/dashboard/views/creditDetailsComponent';
import InvoiceContainer from './screens/dashboard/views/invoiceContainerComponent';
import HeaderComponent from './screens/dashboard/views/headerComponent';

// const Tab = createBottomTabNavigator();

const BottomTab = () => {
  return(
    <View style={styles.bottomTab}>
      <TouchableOpacity>
        <Image source={require('./images/dashboard-colour3x.png')} style={{height: 38, width: 58}} />
      </TouchableOpacity>

      <TouchableOpacity>
        <Image source={require('./images/invoices3x.png')} style={{height: 38, width: 49}} />
      </TouchableOpacity>

      <TouchableOpacity>
        <Image source={require('./images/funds3x.png')} style={{height: 35, width: 49}} />
      </TouchableOpacity>

      <TouchableOpacity>
        <Image source={require('./images/history3x.png')} style={{height: 37, width: 49}} />
      </TouchableOpacity>
    </View>
  );
}

const App: () => React$Node = () => {
  return (
    <SafeAreaView style={styles.bgCopy}>
      <ScrollView>
        <HeaderComponent />

        <InvoiceContainer />
  
        <CreditDetailsComponent />
  
        <InvoiceComponent />

        <BottomTab />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  bgCopy: {
    // objectFit: 'contain',
    backgroundColor: '#f4f4f4',
  },
  bottomTab : {backgroundColor:'#ffffff', flexDirection:'row', justifyContent: 'space-around', alignItems:'center', height: 60}
});

export default App;
