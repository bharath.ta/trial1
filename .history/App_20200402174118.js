/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { 
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TouchableOpacity
} from 'react-native';

//import { NavigationContainer } from '@react-navigation/native';
//import { createStackNavigator } from '@react-navigation/stack';
//import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

/**
 * @desc components for dashboard scree
 */
import InvoiceComponent from './screens/dashboard/invoiceComponent';
import CreditDetailsComponent from './screens/dashboard/creditDetailsComponent';
import InvoiceContainer from './screens/dashboard/invoiceContainerComponent';
import HeaderComponent from './screens/dashboard/headerComponent';

// const Tab = createBottomTabNavigator();

const BottomTab = () => {
  return(
    <View style={styles.bottomTab}>
      <TouchableOpacity>
        <Image source={require('./images/dashboard-colour3x.png')} style={{height: 38, width: 58}} />
      </TouchableOpacity>

      <TouchableOpacity>
        <Image source={require('./images/invoices3x.png')} style={{height: 38, width: 49}} />
      </TouchableOpacity>

      <TouchableOpacity>
        <Image source={require('./images/funds3x.png')} style={{height: 35, width: 49}} />
      </TouchableOpacity>

      <TouchableOpacity>
        <Image source={require('./images/history3x.png')} style={{height: 37, width: 49}} />
      </TouchableOpacity>
    </View>
  );
}

const App: () => React$Node = () => {
  return (
    <SafeAreaView style={styles.bgCopy}>
      <HeaderComponent />
        
      <InvoiceContainer />

      <CreditDetailsComponent />

      <InvoiceComponent />

      <BottomTab />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  bgCopy: {
    // objectFit: 'contain',
    backgroundColor: '#f4f4f4',
    flex:1
  },
  bottomTab : {backgroundColor:'#ffffff', flexDirection:'row', justifyContent: 'space-around', alignItems:'center', height: 60}
});

export default App;
