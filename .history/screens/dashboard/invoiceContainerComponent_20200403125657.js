import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Button,
    Image,
    TouchableOpacity
  } from 'react-native';
  
import string from '../../utils/localizedstrings';

const InvoiceContainer = () => {
  return(
    <View style={styles.RectangleCopy}>
      <TouchableOpacity onPress={()=>alert('add new invoice')} style={} >
        <Image source={require('../../images/group-26copy3x.png')} style={{width: 17, height: 20, marginBottom: 10}}/>
        <Text style={{color:'#fff', fontSize:10}}>{string.addNewInvoice}</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={()=>alert('invoice returns')} style={{justifyContent:'center', alignItems: 'center', marginLeft: 20}} >
        <Image source={require('../../images/group-17copy3x.png')} style={{width: 18, height: 20, marginBottom: 10}}/>
        <Text style={{color:'#fff', fontSize:10}}>{string.invoiceReturns}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  RectangleCopy: {
    width: '90%',
    height: 98,
    borderRadius: 10,
    marginLeft: 20,
    marginRight:20,
    backgroundColor: '#2880b8',
    flexDirection: 'row',
    justifyContent:'center',
    alignItems: 'center',
    shadowOffset: {width:0, height:5},
  },
});

export default InvoiceContainer;