import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image
  } from 'react-native';
import { TouchableOpacity } from 'react-native';
//import string from '../../utils/localizedstrings';

/**
 * @desc this is header component code
 */
const HeaderComponent = () => {
  return(
    <>
      <View style={styles.rectangle}>
        <View style={{flexDirection:'row', paddingLeft:20, alignItems:'center',}}>
          <Text style={{color: '#ffffff', fontFamily:'Helvetica', fontSize: 27.3, letterSpacing: -0.01, paddingRight: 5}}>Actyv</Text>
          <Text style={{color: '#ffffff', fontFamily:'Helvetica', fontSize: 22, letterSpacing: -0.01}}>Pro</Text>
        </View>

        <View style={{flexDirection:'row', alignItems:'center'}}>
          <TouchableOpacity onPress={()=>alert('notifications')}>
            <Image source={require('../../images/icon-notification-copy3.png')} style={{width: 18, height: 18,marginRight: 20}}/>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=>alert('Contacts')}>
            <Image source={require('../../images/icon-profile-copy3.png')} style={{width: 18, height: 18, marginRight:20}}/>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  rectangle: {
      width: '100%',
      height : 50,
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 20,
      backgroundColor: '#2880b8',
      flexDirection: 'row',
      justifyContent: 'space-between',
      margin
    },
});

export default HeaderComponent;