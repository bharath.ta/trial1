import React from 'react';
import {
    StyleSheet,
    View,
    Text,
  } from 'react-native';
import string from '../../utils/localizedstrings';

const CreditDetailsComponent = () => {
  return(
    <>
      <View style={styles.dashboard1}>
        <Text style={styles.dashboard2}>{string.dashboard}</Text>
      </View>

      <View style={styles.RectangleCopy1}>
        <View style={styles.dashboard5}>
          <Text style={{fontSize: 15, letterSpacing: -0.01}}>{string.creditDetails}</Text>
        </View>

        <View style={styles.dashboard4}>

          {/* should change the color for below individual components */}

          <View style={styles.dashboard3}>
            <View style={styles.style1}>
              <Text style={styles.style2}>{string.creditDetails}</Text>
              <Text style={styles.style3}>Rs. 87,00,000</Text>
            </View>
            <View style={styles.style1}>
              <Text style={styles.style2}>FINANCE REQUESTED</Text>
              <Text style={styles.style4}>Rs. 38,12,000</Text>
            </View>
          </View>

          <View style={styles.dashboard3}>
            <View style={styles.style1}>
              <Text style={styles.style2}>CONSUMED LIMIT</Text>
              <Text style={styles.style3}>Rs.3,00,000</Text>
            </View>
            <View style={styles.style1}>
              <Text style={styles.style2}>TOTAL LIMIT</Text>
              <Text style={styles.style3}>Rs.90,00,000</Text>
            </View>
          </View>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  RectangleCopy1: {
    width: '90%',
    height: 223,
    backgroundColor:'#ffffff',
    shadowColor: 'gray',
    shadowOpacity: 0.05,
    shadowRadius: 0.05,
    marginLeft:20,
    marginBottom: 40,
    flexDirection: 'column',
    borderRadius: 20,
  },
  style1: {backgroundColor:'#ffffff',alignItems:'center',justifyContent:'center', flex:1},
  style2: {fontSize:12, color: '#747474'},
  style3: {fontSize:16, color: '#000000', fontWeight: '500'},
  style4: {fontSize:16, color: '#d96100', fontWeight: '500'},
  dashboard1: {paddingTop: 30, paddingLeft: 20, paddingBottom:14,backgroundColor:'#f4f4f4',},
  dashboard2: {fontSize: 20, color:'#2880b8',fontSize:20, fontWeight: '500'},
  dashboard3: {flex:1, flexDirection: "row",},
  dashboard4: {backgroundColor:'#ffffff',flex: 3, flexDirection: "column", borderBottomStartRadius:20, borderBottomEndRadius:20, padding: 5},
  dashboard5: {backgroundColor:'#ffffff',flex: 1, paddingLeft:20 ,justifyContent:'center',borderBottomWidth: 0.3, 
  borderColor:'#ccc', borderTopLeftRadius: 20, borderTopEndRadius: 20}
});

export default CreditDetailsComponent;