import React from 'react';
import { Image, ScrollView, StyleSheet, Text, View, TouchableOpacity } from 'react-native';

const Invoices = props => {
	return(
		<>
			<View style={{paddingLeft:20, paddingRight:20, paddingBottom:20, paddingTop:20}}>
				<Text style={{fontSize: 10, color:'#9b9b9b'}}>{props.date}</Text>

				<View  style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center',}}>
						<View style={{flex:3, paddingRight: 10}}>
								<Text style={{color: '#4a4a4a'}}>{props.company}</Text>
						</View>
						
						<View style={{flex:1}}>
								<Text style={{backgroundColor: '#4cae4a', color: 'white',fontSize:10, width: 33, height: 13, textAlign:'center', borderRadius:6.5}}>{props.tag}</Text>
						</View>
						
						<View style={{flex:1, alignItems:'flex-end'}}>
								<Text style={{color:'#9b9b9b', fontSize: 12}}>{props.days}</Text>
						</View>
				</View>

				<View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center',}}>
						<View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
								<Text style={{color:'#9b9b9b', fontSize:12, paddingRight:5}}>Invoice Amount</Text>
								<Text style={{color:'#4a4a4a', fontSize:12}}>{props.amount}</Text>
						</View>

						<View>
								<Text style={{backgroundColor: props.color, color: 'white', 
								fontSize: 10,textAlign:'center',width: 71, height: 18, padding: 2}}>{props.status}</Text>
						</View>
				</View>
			</View>
	
			<View style={{borderBottomWidth:0.5, borderColor:'#cccccc', width:'90%',alignItems:'center',marginLeft: 20, marginRight: 20}}></View>
		</>
	);
}
const InvoiceComponent = () => {
return(
	<View style={{flex:7}}>
		<View style={{paddingLeft: 20, paddingBottom:5,backgroundColor:'#f4f4f4', marginBottom: 20}}>
				<Text style={{fontSize: 20, color:'#000000',fontSize:16, fontWeight: '500'}}>My Invoices</Text>
		</View>

		<View style={styles.filter}>
			<TouchableOpacity onPress={()=>alert('filter')}>
				<View style={{flexDirection: 'row', marginLeft: 30, alignItems: 'center', justifyContent:'flex-start',marginBottom:10}}>
						<Image source={require('../../images/sort-icon3x.png')} style={{width: 15, height: 15, marginRight:5}}/>
						<Text style={{fontSize:12, marginRight:5, color:'#2880b8', fontWeight:'bold'}}>Filter</Text>
						<Image source={require('../../images/dropdown-icon3x.png')} style={{width: 7, height: 7, marginRight:10}}/>
				</View>
      </TouchableOpacity>
	
			<TouchableOpacity onPress={() => alert('search')}>
				<View style={{flexDirection: 'row', paddingRight: 30, alignItems: 'center', justifyContent:'flex-end',}}>
						<Image source={require('../../images/search3x.png')} style={{width: 15, height: 15}}/>
						{/* <Image source={require('../../images/search.svg')} style={{width: 15, height: 15}}/> */}
						<Text style={{fontSize:12, marginRight:5, color:'#2880b8', fontWeight:'bold', marginLeft: 5}}>Search</Text>
				</View>
			</TouchableOpacity>
		</View>
		
		<View style={styles.Invoice}>
			<ScrollView>
				<Invoices date={'Jan 02, 2020'} company={'Instakart Private Ltd.'} tag={'Aaa'} days={'40 Days'} amount={'Rs.3,50,000'} status={'Pending'} color={'#f7b500'}/>

				<Invoices date={'Jan 01, 2020'} company={'Cloud India Pvt. Ltd.'} tag={'Aaa'} days={'40 Days'} amount={'Rs.84,00,000'} status={'Approved'} color={'#4cae4a'}/>

				<Invoices date={'Jan 02, 2020'} company={'Future Enterprise Ltd.'} tag={'Aaa'} days={'40 Days'} amount={'Rs.1,50,000'} status={'Rejected'} color={'#e02020'}/>

				<Invoices date={'Jan 02, 2020'} company={'Vedanta Meds Public Limited'} tag={'A12'} days={'40 Days'} amount={'Rs.3,50,000'} status={'Pending'} color={'#f7b500'}/>

				<Invoices date={'Jan 02, 2020'} company={'ABC Enterprise1111djdvksdlvsdlvnshvhjhbngbugbjknbhjknbjf'} tag={'A12'} days={'40 Days'} amount={'Rs.3,50,000'} status={'Pending'} color={'#f7b500'}/>
			</ScrollView>   
		</View>
	</View>
	);
}

const styles=StyleSheet.create({
		Invoice:{backgroundColor: '#ffffff', marginTop:5, marginLeft:20, marginRight:20,marginBottom:10, borderRadius: 20, height: 275},
		filter: {height: 25, flexDirection: 'row', backgroundColor:'pink', alignContent:'center', justifyContent: 'space-between', }
});

export default InvoiceComponent;