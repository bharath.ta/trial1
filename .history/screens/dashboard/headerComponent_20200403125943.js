import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image
  } from 'react-native';
import { TouchableOpacity } from 'react-native';
import string from '../../utils/localizedstrings.js';

/**
 * @desc this is header component code
 */
const HeaderComponent = () => {
  return(
    <>
      <View style={styles.rectangle}>
        <View style={{flexDirection:'row', paddingLeft:20, alignItems:'center',}}>
          <Text style={{color: '#ffffff', fontFamily:'Helvetica', fontSize: 23.3, letterSpacing: -0.01, paddingRight: 5,}}>{string.actyv}</Text>
          <Text style={{color: '#ffffff', fontFamily:'Helvetica', fontSize: 20, letterSpacing: -0.01}}>{string.pro}</Text>
        </View>

        <View style={{flexDirection:'row', alignItems:'center'}}>
          <TouchableOpacity onPress={()=>alert('notifications')}>
            <Image source={require('../../images/icon-notification-copy3.png')} style={{width: 16, height: 17,marginRight: 13}}/>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=>alert('Contacts')}>
            <Image source={require('../../images/icon-profile-copy3.png')} style={{width: 16, height: 16, marginRight:20}}/>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  rectangle: {
      width: '100%',
      height : 50,
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 20,
      backgroundColor: '#2880b8',
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginBottom: 16,
    },
});

export default HeaderComponent;