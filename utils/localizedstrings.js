import LocalizedStrings from "react-native-localization";

/**
 * Generating strings for multi language support
 * @constant strings
 */
const strings = new LocalizedStrings({
  en:{
    actyv: 'Actyv',
    pro: 'Pro',
    addNewInvoice: 'ADD NEW INVOICE',
    invoiceReturns: 'INVOICE RETURNS',
    availableLimit: 'AVAILABLE LIMIT',
    financeRequested: 'FINANCE REQUESTED',
    consumedLimit: 'CONSUMED LIMIT',
    totalLimit: 'TOTAL LIMIT',
    dashboard: 'Dashboard',
    creditDetails: 'CREDIT DETAILS',
    invoiceAmount: 'Invoice Amount',
    myInvoices: 'My Invoices',
    filter: 'Filter',
    search: 'Search',
    
  }
});

export default strings;